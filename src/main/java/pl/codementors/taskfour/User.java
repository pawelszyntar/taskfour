package pl.codementors.taskfour;

import java.util.Objects;

public class User implements Comparable<User>{

    private String name;
    private String lastName;
    private String login;
    private Gender gender;

    public User(){
    }

    public User(String name, String lastName, String login, Gender gender) {
        this.name = name;
        this.lastName = lastName;
        this.login = login;
        this.gender = gender;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return ("User {" +
                "name = " + name + ", " +
                "lastName = " + lastName + ", " +
                "login = " + login + ", " +
                "gender = " + gender + "}");
    }

    @Override
    public int compareTo(User other) {
        int ret = lastName.compareTo(other.lastName);
        if (ret == 0) {
            ret = name.compareTo(other.name);
        }
        if (ret == 0) {
            ret = login.compareTo(other.login);
        }
        return ret;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(name, user.name) &&
                Objects.equals(lastName, user.lastName) &&
                Objects.equals(login, user.login) &&
                Objects.equals(gender, user.gender);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, lastName, login, gender);
    }
}
