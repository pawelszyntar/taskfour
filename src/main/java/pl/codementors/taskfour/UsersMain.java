package pl.codementors.taskfour;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class UsersMain {

    public static void main(String[] args) {

        System.out.println("Hello world." + "\n");

        List<User> users = load("/home/pawel/javaKurs/taskfour/userslist");
        System.out.println("Users list:");
        print(users);
        Set<User> usersSet = new HashSet<>();
        usersSet.addAll(users);
        System.out.println();
        System.out.println("Users set:");
        print(usersSet);
        System.out.println();
        System.out.println("Sorted list (lastName ascending order):");
        Collections.sort(users);
        print(users);
        System.out.println();
        System.out.println("Sorted list (login ascending order):");
        Collections.sort(users, new NewClass());
        print(users);
        Set<User> usersTreeSet = new TreeSet<>();
        usersTreeSet.addAll(users);
        System.out.println();
        System.out.println("Users tree set:");
        print(usersTreeSet);

        System.out.println();
        System.out.println("Map:");
        Map<String, User> mapUsers = loadDepartments("/home/pawel/javaKurs/taskfour/userslist2");
        print(mapUsers);
        System.out.println();
        System.out.println("Map list:");
        Map<String, List<User>> mapListUsers = loadDepartments2("/home/pawel/javaKurs/taskfour/userslist2");
        print2(mapListUsers);

        System.out.println();
        System.out.println("Map tree map:");
        Map<String, List<User>> mapTreeMap = new TreeMap<>();
        mapTreeMap.putAll(mapListUsers);
        print2(mapTreeMap);



    }

    public static List<User> load(String fileName) {
        List<User> users = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String readFirstLine = br.readLine();
            int usersCount = Integer.parseInt(readFirstLine);
            for (int i = 0; i < usersCount; i++) {
                String name = br.readLine();
                String lastName = br.readLine();
                String login = br.readLine();
                Gender gender = Gender.valueOf(br.readLine().toUpperCase());
                users.add(new User(name, lastName, login, gender));
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return users;
    }

    public static void print(Collection<User> users) {
        for (User user : users) {
            System.out.println(user);
        }
    }

    public static Map<String, User> loadDepartments(String fileName) {
        Map<String, User> map = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String readFirstLine = br.readLine();
            int usersCount = Integer.parseInt(readFirstLine);
            for (int i = 0; i < usersCount; i++) {
                String name = br.readLine();
                String lastName = br.readLine();
                String login = br.readLine();
                Gender gender = Gender.valueOf(br.readLine().toUpperCase());
                String department = br.readLine();
                map.put(department, new User(name, lastName, login, gender));
            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return map;
    }

    public static void print(Map<String, User> mapUsers) {
        for (String key : mapUsers.keySet()) {
            System.out.println(key + ": " + mapUsers.get(key));
        }
    }

    public static Map<String, List<User>> loadDepartments2(String fileName) {
        Map<String, List<User>> mapNew = new HashMap<>();
        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {
            String readFirstLine = br.readLine();
            int usersCount = Integer.parseInt(readFirstLine);
            for (int i = 0; i < usersCount; i++) {
                String name = br.readLine();
                String lastName = br.readLine();
                String login = br.readLine();
                Gender gender = Gender.valueOf(br.readLine().toUpperCase());
                String department = br.readLine();
                if (!mapNew.containsKey(department)){
                    List<User> userList = new ArrayList<>();
                    mapNew.put(department, userList);
                }
                mapNew.get(department).add(new User(name, lastName, login, gender));

            }
        } catch (IOException ex) {
            System.out.println(ex);
        }
        return mapNew;
    }

    static void print2(Map<String, List<User>> newMap) {
        for (String key : newMap.keySet()) {
            System.out.println(key + ": ");
            for (User element : newMap.get(key)) {
                System.out.println("\t" + element);
            }
        }
    }
}
